﻿using System;
using System.Runtime.Serialization;

namespace PhotosCollaborator.Models.DataContracts
{
	/// <summary>
	/// Klient (użytkownik) systemu. Istnienie obiektu tej klasy jest transparentne dla użytkownika, nie musi on podejmować żadnych akcji, by tenże klient istniał.
	/// </summary>
	[DataContract]
	public class Client
	{
		/// <summary>
		/// Indeks klienta w obrębie danej kolekcji zdjęć.
		/// </summary>
		[DataMember]
		public int Index { get; set; }

		/// <summary>
		/// Unikalna nazwa użytkownika (nadawana automatycznie).
		/// </summary>
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Timestamp ostatniej aktywności.
		/// </summary>
		[DataMember]
		public DateTime LastActiveTimestamp { get; set; }
		
		public override string ToString()
		{
			return String.Format("{0} (last active: {1})", Name, LastActiveTimestamp);
		}
	}
}
