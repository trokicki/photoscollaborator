﻿using System;
using System.Runtime.Serialization;

namespace PhotosCollaborator.Models.DataContracts
{
	/// <summary>
	/// Reprezentacja zdjęcia w systemie.
	/// </summary>
	[DataContract]
	public class Photo
	{
		/// <summary>
		/// Ustala identyfikator zdjęcia jako nowy GUID.
		/// </summary>
		public Photo()
		{
			Guid = GetNewGuid();
		}

		/// <summary>
		/// Nazwa zdjęcia podana przez użytkownika.
		/// </summary>
		[DataMember(Order = 1)]
		public string Name { get; set; }

		/// <summary>
		/// Unikalny identyfikator zdjęcia.
		/// </summary>
		[DataMember(Order = 2)]
		public string Guid { get; set; }

		/// <summary>
		/// Identyfikator autora zdjęcia.
		/// </summary>
		[DataMember(Order = 3)]
		public string AuthorId { get; set; }

		/// <summary>
		/// Stan selekcji zdjęcia - nadaje się do publikacji (1) nie nadaje się (-1) bądź brak oceny (0).
		/// </summary>
		[DataMember(Order = 4)]
		public int Vote { get; set; }

		/// <summary>
		/// Zdjęcie enkodowane jako string w Base64.
		/// </summary>
		[DataMember(Order = 5)]
		public string Base64Content { get; set; }

		public override string ToString()
		{
			return String.Format("({0}) {1}", Guid, Name);
		}

		/// <summary>
		/// Utworzenie nowego identyfikatora.
		/// </summary>
		/// <returns></returns>
		public static string GetNewGuid()
		{
			return System.Guid.NewGuid().ToString().Substring(0, 8).Replace("-", String.Empty);
		}
	}
}
