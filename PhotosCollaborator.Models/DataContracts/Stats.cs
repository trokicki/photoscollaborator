﻿using System;
using System.Runtime.Serialization;

namespace PhotosCollaborator.Models.DataContracts
{
	/// <summary>
	/// Podstawowe statystyki z punktu widzenia użytkownika.
	/// </summary>
	[DataContract]
	public class Stats
	{
		/// <summary>
		/// Ilość aktywnych użytkowników.
		/// </summary>
		[DataMember]
		public int ActiveClientsCount { get; set; }

		/// <summary>
		/// Ilość nieocenionych zdjęć.
		/// </summary>
		[DataMember]
		public int RemainingPhotosCount { get; set; }

		public override string ToString()
		{
			return String.Format("Active users: {0}, remaining photos: {1}", ActiveClientsCount, RemainingPhotosCount);
		}
	}
}
