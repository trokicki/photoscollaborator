﻿using System;
using System.Collections.Generic;
using System.Linq;
using PhotosCollaborator.Models.DataContracts;

namespace PhotosCollaborator.Models.Repositories
{
	/// <summary>
	/// Implementacja sesji aplikacji.
	/// </summary>
	public class ApplicationSession : IApplicationSession
	{
		private readonly IList<Client> _clients;
		private static volatile ApplicationSession _instance = new ApplicationSession();
		private static readonly object Locker = new object();
		public int SessionSecondsTimeout { get { return 20; } }

		/// <summary>
		/// Instancja singletonu.
		/// </summary>
		public static ApplicationSession Instance { get { return _instance; } }

		private ApplicationSession()
		{
			_clients = new List<Client>();
		}

		public IEnumerable<Client> GetClients()
		{
			return _clients;
		}

		public IEnumerable<Client> GetActiveClients()
		{
			var now = DateTime.Now;
			return _clients.Where(c => now.Subtract(c.LastActiveTimestamp).TotalSeconds < SessionSecondsTimeout).AsQueryable();
		}

		public string RegisterNewClient()
		{
			lock (Locker)
			{
				var clientIndex = _clients.Count;
				var clientName = BuildClientName(clientIndex);
				_clients.Add(new Client { Index = clientIndex, Name = clientName, LastActiveTimestamp = DateTime.Now });
				return clientName;
			}
		}

		public Client GetByName(string clientName)
		{
			return _clients.First(c => c.Name == clientName);
		}

		public void PingClientActivity(string clientName)
		{
			GetByName(clientName).LastActiveTimestamp = DateTime.Now;
		}

		public int GetRemainingSessionTime(string clientName)
		{
			return (int)DateTime.Now.Subtract(GetByName(clientName).LastActiveTimestamp).TotalSeconds;
		}

		private static string BuildClientName(int clientIndex)
		{
			var guid = Guid.NewGuid().ToString().Substring(0, 16).Replace("-", String.Empty);
			return String.Format("client_{0}_{1}", clientIndex, guid);
		}
	}
}