﻿using System.Collections.Generic;
using PhotosCollaborator.Models.DataContracts;

namespace PhotosCollaborator.Models.Repositories
{
	/// <summary>
	/// Repozytorium sesji aplikacji (sesja jest odpowiedzialna za przechowywanie m.in. danych dotyczących aktywnych użytkowników).
	/// </summary>
	public interface IApplicationSession
	{
		/// <summary>
		/// Długość sesji w sekundach.
		/// </summary>
		int SessionSecondsTimeout { get; }

		/// <summary>
		/// Pobranie wszystkich użytkowników.
		/// </summary>
		/// <returns>Kolekcja użytkowników.</returns>
		IEnumerable<Client> GetClients();

		/// <summary>
		/// Pobranie aktywnych użytkowników.
		/// </summary>
		/// <returns>Kolekcja użytkowników.</returns>
		IEnumerable<Client> GetActiveClients();

		/// <summary>
		/// Rejestracja nowego użytkownika. Operacja powinna być maksymalnie transparentna, nie są wymagane żadne dane użytkownika.
		/// </summary>
		/// <returns>Automatycznie wygenerowana nazwa użytkownika.</returns>
		string RegisterNewClient();

		/// <summary>
		/// Pobranie użytkownika na podstawie jego unikalnej nazwy.
		/// </summary>
		/// <param name="clientName">Nazwa - klucz.</param>
		/// <returns>Użytkownik.</returns>
		Client GetByName(string clientName);

		/// <summary>
		/// Aktualizacja czasu ostatniej aktywności użytkownika.
		/// </summary>
		/// <param name="clientName">Nazwa aktualizowanego użytkownika.</param>
		void PingClientActivity(string clientName);

		/// <summary>
		/// Zwraca czas do wygaśnięcia sesji użytkownika.
		/// </summary>
		/// <param name="clientName">Identyfikator użytkownika.</param>
		/// <returns>Czas w sekundach.</returns>
		int GetRemainingSessionTime(string clientName);
	}
}
