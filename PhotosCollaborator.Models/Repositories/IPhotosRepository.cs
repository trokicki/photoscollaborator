﻿using System.Linq;
using PhotosCollaborator.Models.DataContracts;

namespace PhotosCollaborator.Models.Repositories
{
	/// <summary>
	/// Interfejs obsługi repozytorium przechowującego zdjęcia.
	/// </summary>
	public interface IPhotosRepository
	{
		/// <summary>
		/// Dodawanie zdjęcia do repozytorium.
		/// </summary>
		/// <param name="photo">Zdjęcie, które ma zostać dodane.</param>
		void Add(ref Photo photo);

		/// <summary>
		/// Pobranie zdjęcia na podstawie unikalnego identyfikatora.
		/// </summary>
		/// <param name="guid">Guid nadany automatycznie przez system.</param>
		/// <returns>Zdjęcie.</returns>
		Photo GetById(string guid);

		/// <summary>
		/// Pobranie wszystkich dostępnych zdjęć.
		/// </summary>
		/// <returns>Kolekcja zdjęć.</returns>
		IQueryable<Photo> GetAll();

		/// <summary>
		/// Oddanie głosu na zdjęcie.
		/// </summary>
		/// <param name="guid">ID zdjęcia.</param>
		/// <param name="vote">+1 lub -1</param>
		void Vote(string guid, int vote);

		/// <summary>
		/// Zliczenie zdjęć, których głos = 0 (zdjęcie nieocenione).
		/// </summary>
		/// <returns>Ilość zdjęć wymagających oceny.</returns>
		int GetRemainingPhotosCount();

		/// <summary>
		/// Pobranie ocenionych pozytywnie zdjęć.
		/// </summary>
		/// <returns>Kolekcja zdjęć.</returns>
		IQueryable<Photo> GetVotedUp();
	}
}
