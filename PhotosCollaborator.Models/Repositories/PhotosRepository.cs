﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using PhotosCollaborator.Models.DataContracts;

namespace PhotosCollaborator.Models.Repositories
{
	/// <summary>
	/// Implementacja repozytorium zdjęć.
	/// </summary>
	public class PhotosRepository : IPhotosRepository
	{
		private readonly Logger _logger = LogManager.GetCurrentClassLogger();

		private readonly IList<Photo> _photos;
		private static readonly object Locker = new object();
		private static volatile PhotosRepository _instance;

		private static readonly TestPhotosProvider TestPhotosProvider = new TestPhotosProvider("TestData");

		/// <summary>
		/// Instancja singletonu.
		/// </summary>
		public static PhotosRepository Instance
		{
			get
			{
				if (_instance != null) return _instance;
				lock (Locker)
				{
					if (_instance == null)
						_instance = new PhotosRepository();
				}
				return _instance;
			}
		}

		private PhotosRepository()
		{
			_photos = new List<Photo>();
			Seed();
		}

		public void Add(ref Photo photo)
		{
			if (!String.IsNullOrEmpty(photo.Guid))
				throw new Exception("Guid must be empty in order to add photo.");
			photo.Guid = Photo.GetNewGuid();
			_photos.Add(photo);
		}

		public Photo GetById(string guid)
		{
			return _photos.FirstOrDefault(p => String.Equals(p.Guid, guid, StringComparison.OrdinalIgnoreCase));
		}

		public IQueryable<Photo> GetAll()
		{
			return _photos.AsQueryable();
		}

		/// <summary>
		/// Oddanie głosu.
		/// </summary>
		/// <param name="photo">Zdjęcie.</param>
		public void Vote(ref Photo photo)
		{
			var persistedPhoto = GetById(photo.Guid);
			var persistedIndex = _photos.IndexOf(persistedPhoto);
			_photos[persistedIndex].Vote = photo.Vote;
		}

		public void Vote(string guid, int vote)
		{
			if (!(vote == -1 || vote == 1))
			{
				throw new Exception("Only +1 or -1 votes are allowed.");
			}
			var photo = GetById(guid);
			photo.Vote = vote;
		}

		public int GetRemainingPhotosCount()
		{
			return _photos.Count(p => p.Vote == 0);
		}

		public IQueryable<Photo> GetVotedUp()
		{
			return _photos.Where(p => p.Vote == 1).AsQueryable();
		}

		private void Seed()
		{
			foreach (var photo in TestPhotosProvider.Seed())
			{
				break;
				_photos.Add(photo);
			}
			_logger.Info("PhotosRepository seeded with test data: {0} photos in total.", _photos.Count);
		}
	}
}
