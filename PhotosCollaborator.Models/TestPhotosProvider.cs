﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NLog;
using PhotosCollaborator.Models.DataContracts;

namespace PhotosCollaborator.Models
{
#pragma warning disable 1591
	public class TestPhotosProvider

	{
		private readonly Logger _logger = LogManager.GetCurrentClassLogger();
		private static readonly Random Random = new Random();

		private readonly string _directoryPath;

		public TestPhotosProvider(string directoryPath)

		{
			_directoryPath = directoryPath;
		}

		// ReSharper disable once CSharpWarnings::CS1591
		public IEnumerable<Photo> Seed()
		{
			if (Directory.Exists(_directoryPath))
				return Directory.EnumerateFiles(_directoryPath, "*.txt").Select(f => new Photo
				{
					Guid = Photo.GetNewGuid(),
					Name = String.Format("{0}_{1}", Path.GetFileNameWithoutExtension(f), BuildRandomString(8)),
					Base64Content = File.ReadAllText(f)
				});

			_logger.Warn("Provided directoryPath for test data not found. Path: {0} FullPath: {1}", _directoryPath, Path.GetFullPath(_directoryPath));
			return Enumerable.Empty<Photo>();
		}

		private static string BuildRandomString(int length)
		{
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var result = new string(
				Enumerable.Repeat(chars, length)
						  .Select(s => s[Random.Next(s.Length)])
						  .ToArray());
			return result;
		}
	}
#pragma warning restore 1591
}
