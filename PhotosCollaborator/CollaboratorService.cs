﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using PhotosCollaborator.Models.DataContracts;
using PhotosCollaborator.Models.Repositories;

namespace PhotosCollaborator
{
	/// <summary>
	/// Implementacja usługi do kolaboracji przy selekcji zdjęć.
	/// </summary>
	public class CollaboratorService : CorsCompliantServiceBase, ICollaboratorService
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
		private static readonly IPhotosRepository PhotosRepository = Models.Repositories.PhotosRepository.Instance;
		private static readonly IApplicationSession ApplicationSession = Models.Repositories.ApplicationSession.Instance;

		public void GetOptions()
		{
		}

		public string Register()
		{
			var clientName = ApplicationSession.RegisterNewClient();
			ApplicationSession.PingClientActivity(clientName);
			var activeClientsCount = ApplicationSession.GetActiveClients().Count();
			var totalClientsCount = ApplicationSession.GetClients().Count();
			Logger.Info("User {0} registered. Active clients: {1}, total: {2}", clientName, activeClientsCount, totalClientsCount);
			return clientName;
		}

		public void AddPhoto(Photo photo, string clientName)
		{
			SavePhoto(photo, clientName);
		}

		public void AddPhotos(IEnumerable<Photo> photos, string clientName)
		{
			var photosList = photos as IList<Photo> ?? photos.ToList();
			Logger.Info("Adding {0} photos.", photosList.Count);
			foreach (var photo in photosList)
			{
				SavePhoto(photo, clientName);
			}
		}

		public void MarkPhoto(string guid, string vote, string clientName)
		{
			var client = ApplicationSession.GetByName(clientName);
			var parsedVote = Int32.Parse(vote);
			ApplicationSession.PingClientActivity(clientName);
			Logger.Info("Photo {0} was voted {1} by {2}", guid, parsedVote == 1 ? "UP" : parsedVote == -1 ? "DOWN" : "", client);
			var photo = PhotosRepository.GetById(guid);
			if (photo.Vote != 0)
			{
				Logger.Info("Photo {0} was already voted {1}", photo.Guid, photo.Vote == 1 ? "UP" : "DOWN");
			}
			else
			{
				PhotosRepository.Vote(guid, parsedVote);
			}
		}

		public IEnumerable<Photo> GetPhotosPack(string clientName)
		{
			var photosCount = PhotosRepository.GetAll().Count(p => p.Vote == 0);
			var activeClients = ApplicationSession.GetActiveClients().OrderBy(c => c.Index).ToList();
			var activeClientsCount = activeClients.Count;
			var client = activeClients.First(c => c.Name == clientName);
			var clientMappedIndex = activeClients.IndexOf(client);
			var photosPerClientCount = activeClientsCount > 0 ? photosCount / activeClientsCount : 0;
			var firstPhotoIndex = clientMappedIndex * photosPerClientCount;
			ApplicationSession.PingClientActivity(client.Name);
			Logger.Info("Returning unvoted photos from {0} to {1} of {2} for client {3}.", 
				firstPhotoIndex, 
				firstPhotoIndex + photosPerClientCount, 
				photosCount, 
				client);
			return PhotosRepository.GetAll().Where(p => p.Vote == 0).Skip(firstPhotoIndex).Take(photosPerClientCount);
		}

		public IEnumerable<Photo> GetAllPhotos()
		{
			var allPhotos = PhotosRepository.GetAll().ToList();
			Logger.Info("Returning all ({0}) photos.", allPhotos.Count);
			return allPhotos;
		}

		public IEnumerable<Photo> GetVotedUpPhotos()
		{
			var votedUpPhotos = PhotosRepository.GetVotedUp().ToList();
			Logger.Info("Returning voted up ({0}) photos.", votedUpPhotos.Count);
			return votedUpPhotos;
		}

		public Stats GetStats()
		{
			var stats = new Stats
			{
				RemainingPhotosCount = PhotosRepository.GetRemainingPhotosCount(),
				ActiveClientsCount = ApplicationSession.GetActiveClients().Count()
			};
			Logger.Info("Statistics: {0}", stats);
			return stats;
		}

		public int PingUserSession(string clientName)
		{
			ApplicationSession.PingClientActivity(clientName);
			return ApplicationSession.SessionSecondsTimeout;
		}

		public int GetRemainingSessionTime(string clientName)
		{
			return ApplicationSession.GetRemainingSessionTime(clientName);
		}

		private static void SavePhoto(Photo photo, string clientName)
		{
			Logger.Info("Adding photo: {0}", photo);
			photo.AuthorId = clientName;
			PhotosRepository.Add(ref photo);
			ApplicationSession.PingClientActivity(clientName);
		}
	}
}
