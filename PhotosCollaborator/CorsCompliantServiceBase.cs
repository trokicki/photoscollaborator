﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace PhotosCollaborator
{
	/// <summary>
	/// Klasa bazowa dla usług wspierających CORS.
	/// </summary>
	public abstract class CorsCompliantServiceBase
	{
		/// <summary>
		/// Podstawowy konstruktor.
		/// </summary>
		protected CorsCompliantServiceBase()
		{
			SetCorsHeaders();
		}

		/// <summary>
		/// Ustawienie nagłówków na potrzeby CORS.
		/// </summary>
		protected void SetCorsHeaders()
		{
			if (WebOperationContext.Current == null) return;
			WebOperationContext.Current.OutgoingResponse.Headers["Access-Control-Allow-Origin"] = "*";
			WebOperationContext.Current.OutgoingResponse.Headers["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS";
			WebOperationContext.Current.OutgoingResponse.Headers["Access-Control-Allow-Headers"] = "Content-Type";
		}
	}
}
