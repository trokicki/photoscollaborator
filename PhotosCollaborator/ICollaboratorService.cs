﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using PhotosCollaborator.Models.DataContracts;

namespace PhotosCollaborator
{
	/// <summary>
	/// Usługa sieciowa usprawniająca kolaborację przy selekcji zdjęć. Każdy użytkownik (użytkownikiem jest osoba mająca fizyczny dostęp do systemu) dodaje zdjęcia, następnie system automatycznie dzieli zdjęcia pomiędzy aktywnych użytkowników by każdy z nich mógł we własnym zakresie dokonać selekcji. Ostatecznie zdjęcia są grupowane celem utworzenia zbiorczej kolekcji.
	/// </summary>
	[ServiceContract]
	public interface ICollaboratorService
	{
		/// <summary>
		/// Odpowiedź na zapytanie z verbem OPTIONS (wykorzystywane na potrzeby CORS).
		/// </summary>
		[OperationContract]
		[WebInvoke(Method = "OPTIONS", UriTemplate = "*")]
		void GetOptions();

		/// <summary>
		/// Rejestracja nowego użytkownika.
		/// </summary>
		/// <returns>Unikalna nazwa wykorzystywana do przyszłej autentykacji użytkownika.</returns>
		[OperationContract]
		[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
		string Register();

		/// <summary>
		/// Dodanie zdjęcia.
		/// </summary>
		/// <param name="photo">Dodawane zdjęcie.</param>
		/// <param name="clientName">Nazwa użytkownika dodającego zdjęcie.</param>
		[OperationContract]
		[WebInvoke(Method = "POST", UriTemplate = "AddPhoto/{clientName}")]
		void AddPhoto(Photo photo, string clientName);


		/// <summary>
		/// Dodanie wielu zdjęć.
		/// </summary>
		/// <param name="photos">Kolekcja zdjęć.</param>
		/// <param name="clientName">Nazwa klienta.</param>
		[OperationContract]
		[WebInvoke(Method = "POST", UriTemplate = "AddPhotos/{clientName}", RequestFormat = WebMessageFormat.Json)]
		void AddPhotos(IEnumerable<Photo> photos, string clientName);

		/// <summary>
		/// Otagowanie zdjęcia według dostępnej skali ocen.
		/// </summary>
		/// <param name="photoGuid"></param>
		/// <param name="vote"></param>
		/// <param name="clientName">Nazwa użytkownika tagującego.</param>
		[OperationContract]
		[WebInvoke(Method = "POST", UriTemplate = "MarkPhoto/{clientName}/{photoGuid}/{vote}")]
		void MarkPhoto(string photoGuid, string vote, string clientName);

		/// <summary>
		/// Pobranie zakresu zdjęć do selekcji dla danego użytkownika.
		/// </summary>
		/// <param name="clientName">Nazwa użytkownika, który ma dokonać wyboru zdjęć.</param>
		/// <returns>Kolekcja zdjęć do selekcji.</returns>
		[OperationContract]
		[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetPhotosPack/{clientName}")]
		IEnumerable<Photo> GetPhotosPack(string clientName);

		/// <summary>
		/// Pobranie wszystkich zdjęć.
		/// </summary>
		/// <returns>Ostateczna kolekcja zdjęć.</returns>
		[OperationContract]
		[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAllPhotos")]
		IEnumerable<Photo> GetAllPhotos();

		/// <summary>
		/// Pobranie wszystkich zdjęć.
		/// </summary>
		/// <returns>Ostateczna kolekcja zdjęć.</returns>
		[OperationContract]
		[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVotedUpPhotos")]
		IEnumerable<Photo> GetVotedUpPhotos();

		/// <summary>
		/// Odświeżenie sesji klienta.
		/// </summary>
		/// <param name="clientName">Identyfikator klienta.</param>
		/// <returns>Czas trwania sesji w sekundach.</returns>
		[OperationContract]
		[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "PingUserSession/{clientName}")]
		int PingUserSession(string clientName);

		/// <summary>
		/// Pobranie statystyki.
		/// </summary>
		/// <returns>Statystyki.</returns>
		[OperationContract]
		[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetStats")]
		Stats GetStats();

		/// <summary>
		/// Pobranie pozostałego czasu sesji.
		/// </summary>
		/// <param name="clientName">Nazwa klienta.</param>
		/// <returns>Czas w sekundach.</returns>
		[OperationContract]
		[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetRemainingSessionTime/{clientName}")]
		int GetRemainingSessionTime(string clientName);
	}
}
