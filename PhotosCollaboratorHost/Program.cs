﻿using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Description;
using PhotosCollaborator;

namespace PhotosCollaboratorHost
{
	class Program
	{
		static void Main(string[] args)
		{
			var address = args.Any() ? args[0] : "http://localhost:8000/PhotosCollaborator/";
			Console.WriteLine("Starting service at {0}", address);
			var baseUri = new Uri(address);
			var webServiceHost = new WebServiceHost(typeof(CollaboratorService), baseUri);
			
			try
			{
				var webHttpBinding = new WebHttpBinding { CrossDomainScriptAccessEnabled = true };
				webServiceHost.AddServiceEndpoint(typeof(ICollaboratorService), webHttpBinding, "");
				webServiceHost.Description.Behaviors.Add(new ServiceMetadataBehavior { HttpGetEnabled = true });
				webServiceHost.Open();
				Console.WriteLine("Service started at {0}", baseUri);

				do
				{
					Console.WriteLine("Press q to exit.");
				} while (Console.ReadKey().KeyChar != 'q');

				webServiceHost.Close();
				Console.WriteLine("Service stopped.");
			}
			catch (Exception e)
			{
				Console.WriteLine("Exception: {0}", e);
				webServiceHost.Abort();
			}
		}
	}
}
