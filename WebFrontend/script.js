(function() {
	var userName = undefined;
		
	$("#connectToRemoteButton").click(function(){
		var remoteAddress = $("#remoteAddressInput").val() + "Register";
		$.getJSON(remoteAddress, function( data ) {
			userName = data;
			$("#connectionStatusLabel").text("Połączony jako: " + userName);
		}).done(function(){
			appViewModel.pingSession();
		});
	});
	
	var BuildRemoteUrl = function (method) {
		return $("#remoteAddressInput").val() + method + "/";
	}
	
	var DownloadAllPhotos = function(){
		var remoteAddress = $("#remoteAddressInput").val() + "GetVotedUpPhotos";
		$.getJSON(remoteAddress, function( data ) {
			appViewModel.allPhotos.photos.removeAll();
			for(var i=0; i<data.length; i++){
				var photo = new PhotoViewModel(data[i].Name);
				photo.base64content(data[i].Base64Content);
				photo.identifier(data[i].Guid);
				appViewModel.allPhotos.photos.push(photo);
			}
		});
	}
	
	var DownloadPhotosPack = function(){
		if(!userName){
			alert("Brak połączenia. Połącz się korzystając z formularza powyżej.");
			return;
		}
		var remoteAddress = BuildRemoteUrl("GetPhotosPack") + userName;
		$.getJSON(remoteAddress, function( data ) {
			appViewModel.downloadedPhotosPack.photos.removeAll();
			for(var i=0; i<data.length; i++){
				var photo = new PhotoViewModel(data[i].Name);
				photo.base64content(data[i].Base64Content);
				photo.identifier(data[i].Guid);
				appViewModel.downloadedPhotosPack.photos.push(photo);
			}
		});
		appViewModel.pingSession();
	}
	
	var PhotoViewModel = function(name){
		var self = this;
		self.name = ko.observable(name);
		self.vote = ko.observable(0); //1 - upvote, -1 - downvote, 0 - undefined
		self.base64content = ko.observable("");
		self.identifier = ko.observable("");
	};

	var PhotosToBeUploadedViewModel = function(){
		var self = this; 
		self.photosToBeUploaded = ko.observableArray();
		self.photoName = ko.observable("");
		self.photoBase64content = ko.observable("");
		
		self.addPhoto = function(){
			if(self.photoName() != ""){
				var photo = new PhotoViewModel(self.photoName());
				photo.base64content(self.photoBase64content()); 
				self.photosToBeUploaded.push(photo);
				self.photoName("");
				self.photoBase64content("");
			}
		};
		
		self.uploadPhotos = function(){		
			if(!userName){
				alert("Brak połączenia. Połącz się korzystając z formularza powyżej.");
				return;
			}
			var photos = [];
			for(var i=0; i<self.photosToBeUploaded().length; i++){
				var photo = {
					Name: self.photosToBeUploaded()[i].name(),
					Base64Content: self.photosToBeUploaded()[i].base64content()
				};
				photos.push(photo);
			}
			var remoteAddress = BuildRemoteUrl("AddPhotos") + userName;
			$.ajax({
				url: remoteAddress, type: "POST", 
				dataType: "json",
				contentType: "application/json",
				data: JSON.stringify(photos)
			}).always(function(){
				self.photosToBeUploaded.removeAll();
			});
			appViewModel.pingSession();
		}
	};
	
	var DownloadedPhotosPackViewModel = function(){
		this.photos = ko.observableArray();
	};
	
	var AllPhotosViewModel = function(){
		this.photos = ko.observableArray();
	}
	
	var PingSession = function () {
		if (!userName) {
			alert("Brak połączenia. Połącz się korzystając z formularza powyżej.");
			return;
		}
		var remoteAddress = BuildRemoteUrl("PingUserSession") + userName;
		$.getJSON(remoteAddress).done(function (data) {
			appViewModel.sessionRemainingTime(parseInt(data));
		});
	}

	var CheckRemainingSessionTime = function(){
		if (userName) {
			var remoteAddress = BuildRemoteUrl("GetRemainingSessionTime") + userName;
			$.getJSON(remoteAddress).done(function (data) {
				appViewModel.sessionRemainingTime(parseInt(data));
			});
		}
	}

	var GetStats = function(){
		var remoteAddress = $("#remoteAddressInput").val() + "GetStats";
		$.getJSON(remoteAddress).done(function (data) {
			appViewModel.remainingPhotosCount(data.RemainingPhotosCount);
			appViewModel.activeClientsCount(data.ActiveClientsCount);
		});
	}
	
	var VotePhoto = function(photo, mark){
		if(!userName){
			alert("Brak połączenia. Połącz się korzystając z formularza powyżej.");
			return;
		}
		var remoteAddress = $("#remoteAddressInput").val() + "MarkPhoto/" + userName + "/" + photo.identifier() + "/" + mark;
		$.ajax({
			url: remoteAddress, type: "POST", 
			dataType: "json",
			contentType: "application/json",
			data: JSON.stringify("")
		});
		appViewModel.pingSession();
		appViewModel.downloadedPhotosPack.photos.remove(photo);
	}
	
	var AppViewModel = function(){
		var self = this;
		self.photosToBeUploaded = new PhotosToBeUploadedViewModel();
		self.downloadedPhotosPack = new DownloadedPhotosPackViewModel();
		self.allPhotos = new AllPhotosViewModel();
		self.sessionRemainingTime = ko.observable(0);
		self.activeClientsCount = ko.observable(0);
		self.remainingPhotosCount = ko.observable(0);

		self.getStats = GetStats;
		self.downloadAllPhotos = DownloadAllPhotos;
		self.downloadPhotosPack = DownloadPhotosPack;
		self.pingSession = PingSession;
		self.checkRemainingSessionTime = CheckRemainingSessionTime;
		
		self.buildImageUrl = function(photoViewModel){
			var computed = "data:image/png;base64," + photoViewModel.base64content();
			return computed;
		};
		
		self.votePhotoUp = function(photo){
			VotePhoto(photo, 1);
		}
		
		self.votePhotoDown = function(photo){
			VotePhoto(photo, -1);
		}

		self.fadeVotedPhotoOut = function(item){
			$(item).fadeOut();
		}
	};

	var appViewModel = new AppViewModel();
	ko.applyBindings(appViewModel);

	var sessionTimeIntervalHandler = setInterval(function(){
		if(appViewModel.sessionRemainingTime() > 0){
			appViewModel.sessionRemainingTime(appViewModel.sessionRemainingTime() - 1);
		}
	}, 1000);

})();



